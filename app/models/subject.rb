class Subject < ActiveRecord::Base
    has_many :tasks
    has_and_belongs_to_many :timetable_days
end
