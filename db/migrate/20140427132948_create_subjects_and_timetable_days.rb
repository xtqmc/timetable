class CreateSubjectsTimetableDays < ActiveRecord::Migration
    def change
        create_table :subjects_timetable_days do |t|
            t.student_id, :subjects
            t.course_id, :timetable_days
            
            t.timestamps
        end
    end
end