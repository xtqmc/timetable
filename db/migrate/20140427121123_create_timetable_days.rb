class CreateTimetableDays < ActiveRecord::Migration
    def change
        create_table :timetable_days do |t|
            t.string :day
            t.text :class_order
            
            t.timestamps
        end
    end
end
