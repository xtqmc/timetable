class AddSubjectIdToTasks < ActiveRecord::Migration
    def self.up
        add_column :tasks, :subject_id, :integer
    end
end