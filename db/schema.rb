# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140430005915) do

  create_table "subjects", force: true do |t|
    t.string   "name"
    t.string   "teacher"
    t.string   "classroom"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subjects_timetable_days", id: false, force: true do |t|
    t.integer "subject_id",       null: false
    t.integer "timetable_day_id", null: false
  end

  add_index "subjects_timetable_days", ["subject_id"], name: "index_subjects_timetable_days_on_subject_id", using: :btree
  add_index "subjects_timetable_days", ["timetable_day_id"], name: "index_subjects_timetable_days_on_timetable_day_id", using: :btree

  create_table "tasks", force: true do |t|
    t.string   "name"
    t.text     "notes"
    t.datetime "due_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "subject_id"
  end

  create_table "timetable_days", force: true do |t|
    t.string   "day"
    t.text     "class_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
